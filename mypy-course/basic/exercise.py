class Rock(object):
    @staticmethod
    def break_the_scissors():
        print('\tRock: Breaking the scissors')


class Paper(object):
    @staticmethod
    def wrap_the_rock():
        print("\tPaper: Wrapping the rock")


class Scissors(object):
    @staticmethod
    def cut_the_paper():
        print("\tScissors: Cutting the paper")


THE_ONLY_OBJECTS_KNOWN_BY_HUMAN_BEING = [Rock, Paper, Scissors]


class BrokenTelephone(object):
    pass


class Person(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self, other=None):
        print('Hello ! My name is {name}'.format(name=self.name))
        if other:
            print('How are you {other_name}?'.format(other_name=other.name))
            if other.age > 60:
                print('Do you need help with those bags??')

    def pick_up_something_from_the_floor(self, random_thing):
        """
        :param random_thing: Some random thing. We can't know for sure what could it be...
        :return: True if the Person knows the random_thing type
            (False if the person have never seen something like this)
        """
        if type(random_thing) not in THE_ONLY_OBJECTS_KNOWN_BY_HUMAN_BEING:
            print('I don\'t know what a "{unknown_type}" is...'.format(unknown_type=type(random_thing).__name__))
            return False
        else:
            self._do_something_with_that_known_object(random_thing)
            return True

    @staticmethod
    def _do_something_with_that_known_object(known_object):
        print('Hey, what a beautiful {thing_type}'.format(thing_type=type(known_object).__name__))
        relevant_method = get_relevant_method_from_object(known_object)
        return relevant_method()


def get_relevant_method_from_object(input_object):
    object_type = type(input_object)
    if object_type == Rock:
        return input_object.break_the_scissors
    elif object_type == Paper:
        return input_object.wrap_the_rock
    elif object_type == Scissors:
        return input_object.cut_the_paper
    raise TypeError("input object is not in OBJECT_LIST_KNOWN_BY_HUMAN_BEING !!!")


def main():
    person = Person("Dwayne Johnson", 1)
    person.say_hello()
    for random_object in [Rock(), Paper(), Scissors(), BrokenTelephone()]:
        person.pick_up_something_from_the_floor(random_object)


if __name__ == '__main__':
    main()
