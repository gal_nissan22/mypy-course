# Hints:
* Checkout the module ``typing`` and ``mypy`` documentation
### How to annotate a function?
* use this syntax (python2 and also python3 compatible)

``# type: (PARAMETERS_TYPES) -> RETURN_TYPE``

for example

    def sum(x, y):
        # type: (int, int) -> int
        return x + y

---

### Special types that you MUST know:
* ``None``: sometimes your function does not return anything...
* ``typing.Union``: lets you annotate types with more than one option.
* ``typing.Optional``: a ``Union`` of ***some-types*** and ``None``. We can add ``None`` to our
``Union`` types, but this is more elegant. NEVER USE ``Union[..., None]``
* ``typing.Any``: accepts any-type (make **mypy** pass this untyped context as typed...)
  * should rarely be necessary in production code - most of the time you can at least specify
  a ``Union`` of possible types
  * ``Any`` ***disables*** any further checking of that value

  * #####You should have a very good reason to use ``Any`` !
* ``typing.Callable``: when your param / return value is a function... check out the
documentation (typing module)
