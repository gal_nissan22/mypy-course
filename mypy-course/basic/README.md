### Basic:

Hey !
Welcome to the first exercise: the Basic level !
Your mission is to make the ``exercis.py`` file to pass ``MyPy`` test:

    mypy --py2 exercise.py --strict

You shouldn't add code that is not for typing.

This file has the **SAME-CODE** as the ``solution.py``, except for typing.

* If you get stuck, you can use the ``HINTS.md`` file.

Good Luck :)
