from typing import TypeVar, Generic, List

T = TypeVar('T')


class Stack(Generic[T]):
    def __init__(self):
        # type: () -> None
        self.items = []     # type: List[T]

    def push(self, item):
        # type: (T) -> None
        self.items.append(item)

    def pop(self):
        # type: () -> T
        return self.items.pop()

    def empty(self):
        # type: () -> bool
        return not self.items


if __name__ == '__main__':
    stack = Stack[int]()
    stack.push(2)
    print(stack.pop())
    print(stack.empty())
