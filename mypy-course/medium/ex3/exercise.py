class Stack(object):
    def __init__(self):
        """
        Once an instance implemented, the other items of the Stack should be the same type.

        example:
            # this code should fail mypy (TypeError)
            stack = Stack()
            stack.push(100)
            stack.push('Hello')
        """
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def empty(self):
        return not self.items


if __name__ == '__main__':
    stack = Stack[int]()
    stack.push(2)
    print(stack.pop())
    print(stack.empty())
