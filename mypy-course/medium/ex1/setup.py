from setuptools import setup, find_packages

setup(
    name='ex1',
    description='mypy-course, medium-level, exercise 1',
    version='1.0.0',
    packages=find_packages(),
    requires=['typing', 'six'],
    author='mypy-course'
)
