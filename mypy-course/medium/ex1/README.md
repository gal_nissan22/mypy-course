# Import Loops:
### Before starting this exercise:
* In this exercise, the ``exercise/`` and the ``solution/`` are stored as packages,
in the ``ex1/`` package.
* Before doing all the cyber, run this:


    cd mypy-course\medium\ex1
    pip install -e .

### Let's Get Started !
* Sometimes, typing our code can cost us import-loops.
* In this exercise you have 2 goals:
  * make mypy run successfully:
    * ``mypy --py2 --strict planet.py superman.py``
  * run superman.py file successfully
    * ``python superman.py``
* ###### Note: we already added typing for part of the code because it is less relevant to the exercise :)

Good Luck :)