from six import add_metaclass
from typing import Dict, Any


class Singleton(type):
    _instances = {}     # type: Dict[Any, Any]

    def __call__(cls, *args, **kwargs):
        # type: (Any, Any) -> Any
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


@add_metaclass(Singleton)
class Superman(object):
    def __init__(self):
        pass

    def save_the_world_again(self, planet):
        pass
