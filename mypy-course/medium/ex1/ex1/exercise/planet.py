from ex1.exercise.superman import Superman


class Planet(object):
    def __init__(self):
        pass

    def _call_superman(self):
        superman = Superman()
        superman.save_the_world_again(self)

    def save_planet_from_aliens(self):
        self._call_superman()
