from ex1.solution.superman import Superman


class Planet(object):
    def __init__(self):
        # type: () -> None
        pass

    def _call_superman(self):
        # type: () -> None
        superman = Superman()
        superman.save_the_world_again(self)

    def save_planet_from_aliens(self):
        # type: () -> None
        self._call_superman()
