from time import time, sleep


def time_test(original_func):
    def timed_func(*args, **kwargs):
        start_time = time()
        result = original_func(*args, **kwargs)
        end_time = time()
        print "Function Name: {}\n" \
              "Args: {}\n" \
              "Kwargs: {}\n" \
              "Result: {}\n" \
              "Execution Time: {}".format(original_func.__name__, args, kwargs, result, start_time - end_time)
        return result
    return timed_func


@time_test
def goo(x, y):
    sleep(0.1)
    return x * y


if __name__ == '__main__':
    goo(1, 2)
    goo(3, "Hello")     # should cause a typeError (at mypy run)
