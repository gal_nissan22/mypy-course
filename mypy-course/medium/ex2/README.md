# Decorators:
* In this exercise you will learn how to add typing to decorators !
  * ###### note that decorators should NOT hide the original function annotation !
* Your mission is to run the mypy test:
  * ``mypy --py2 --strict exercise.py``
  * ###### not that the second call to the function ``goo`` should cause a typeError:
* The mypy-test output should be:
  

    solution.py:<Some-Line>: error: Argument 2 to "goo" has incompatible type "str"; expected "int"
    Found 1 error in 1 file (checked 1 source file)


Good Luck :)