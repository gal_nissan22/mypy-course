# Hints:
* This file continues the basic/HINTS.md tutorial.
### typing.TYPE_CHECKING
* Read the documentation of ``typing`` module.
* In short:
  * ``typing.TYPE_CHECKING`` is ``False`` at runtime, and is ``True`` on type-checking
    (for example **MyPy** run)


### typing.TypeVar
* Read the documentation...
* In short:
  * ``TypeVar`` refers to the question:
    * If I want to accept ``Any`` type, but I want to return the same ``"Any"`` type that I got, what should I do?
      * ``# type(Any) -> Any`` won't work... think why
  * We use ``TypeVar`` when we want to use a generic type more than once.
  * What is the ``bound`` argument?


### Decorators with MyPy:
* Read mypy documentation about "Declaring decorators"
  * After reading it, think about a nice way to type the wrapper function (a generic function that we want to work with
any type...)


### typing.Callable:
* In order to type a generic ``Callable``, you can use ``Callable[..., ReturnType]``
  * If you want to use a plain ``Callable``, use ``Callable[..., Any]`` or else mypy --strict would say:
  

    Missing type parameters for generic type "Callable"


### Generics:
* Read ``mypy`` documentation